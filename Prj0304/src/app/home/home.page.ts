import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "App motos";

  cards = [
  {
    titulo: "KTM 1290 SUPER DUKE RR 2021",
    subtitulo: "First moto",
    conteudo: "Baseado no mais recente PROTÓTIPO KTM 1290 SUPER DUKE R, o KTM 1290 SUPER DUKE RR é a própria definição de READY TO RACE. 9 kg mais leve que a KTM 1290 SUPER DUKE R padrão, com uma relação peso/potência de 1:1 e carroceria de fibra de carbono ultraexclusiva e leve, a KTM 1290 SUPER DUKE RR é uma novíssima BEAST com um traço ainda mais cruel. ",
    foto: "https://www.motorevistacr.com/wp-content/uploads/2019/11/KTM-1290-SUPER-DUKE-R-2020.jpg",
    adicionais:"22K",
    adicionais2:"250K",
    adicionais3:"1.000.000.000"
  },
  {
    titulo: "MV Agusta Rivale 800",
    subtitulo: "Second moto",
    conteudo: "A MV Agusta Rivale 800 é a quintessência de uma motocicleta italiana. Feita para ser estilosa e divertida, o radical modelo deixa de lado a praticidade e reúne um design deslumbrante com o alto desempenho de seu motor tricilíndrico de 125 cv de potência máxima. Dotada de um conjunto ciclístico que garante boa maneabilidade, a Rivale incorpora itens exclusivos.",
    foto: "http://www.asphaltandrubber.com/wp-content/gallery/2014-mv-agusta-rivale-800-ride-review/2014-mv-agusta-rivale-800-33.jpg",
    adicionais:"20.5K",
    adicionais2:"225K",
    adicionais3:"950.000.000"
  },
  {
    titulo: "BMW K 1600 GTL",
    subtitulo: "Third moto",
    conteudo: "Pilote e deslize a caminho do horizonte com estilo e elegância ao guidão desta luxuosa moto touring de alto desempenho. A nova BMW K 1600 GTL é o apogeu da qualidade e do conforto absolutos, que complementam o lendário motor de seis cilindros em linha, e que agora ainda traz a mais recente geração do Dynamic ESA e ABS Pro como itens de série. ",
    foto: "https://www.motorfreaks.nl/images/stories/reviews/BMW/2014/K1600_GTL/BMW_K1600_GTL_Exclusive_21.JPG",
    adicionais:"19K",
    adicionais2:"190K",
    adicionais3:"850.000.000"
  },
  {
    titulo: "Vespa 946 Emporio Armani",
    subtitulo: "Fourth moto",
    conteudo: "Duas grifes italianas, extremo requinte e a exclusividade de apenas dez mil unidades produzidas. Assim é a Vespa 946 Giorgio Armani,  lançada em versão limitada no ano passado para comemorar os 130 anos da marca de scooters e os 40 anos da grife de roupas. O Brasil recebeu apenas 30 unidades da icônica Vespa e com valores astronômicos.",
    foto: "https://i.pinimg.com/originals/05/b7/b0/05b7b0dfb3fc04776e7682e5138b2f66.jpg",
    adicionais:"20K",
    adicionais2:"215K",
    adicionais3:"900.000.000"
  }  
  ];
  constructor() {}

}
