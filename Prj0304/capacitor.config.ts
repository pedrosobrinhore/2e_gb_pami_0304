import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Prj0304',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
